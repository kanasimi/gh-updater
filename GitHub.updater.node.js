/**
 * @name gh-updater. GitHub repository auto-updater, and auto-update CeJS via
 *       GitHub. GitHub repository 自動更新工具 / 自動配置好最新版本 CeJS 程式庫的工具。
 * 
 * @fileoverview 將會採用系統已有的 7-Zip 程式，自動取得並解開 GitHub 最新版本 repository zip
 *               壓縮檔案至當前工作目錄下 (e.g., ./CeJS-master)。
 * 
 * @example<code>

TODO:
use Zlib

 </code>
 * 
 * @since 2017/3/13 14:39:41 初版<br />
 *        2018/8/20 12:52:34 改寫成 GitHub 泛用的更新工具 GitHub Upgrade Tool，並將
 *        _CeL.path.txt → _repository_path_list.txt<br />
 *        2018/8/30 20:17:7 增加 target_directory 功能。<br />
 *        2018/9/8 18:29:29 Create npm package: gh-updater, _CeL.updater.node.js →
 *        gh-updater/GitHub.updater.node.js
 */

'use strict';

// --------------------------------------------------------------------------------------------
// setup. 設定區。

var default_repository_path = 'kanasimi/CeJS', extract_program_path = [ '7z',
// e.g., install p7zip package via yum
'7za', 'unzip', '"C:\\Program Files\\7-Zip\\7z.exe"' ],

// modify from _CeL.loader.nodejs.js
repository_path_list_file = './_repository_path_list.txt',
/** {String}CeJS 更新工具相對於 CeJS 根目錄的路徑。 e.g., "CeJS-master/_for include/". const */
default_update_script_directory = '_for include/',

// const
node_https = require('https'), node_fs = require('fs'), child_process = require('child_process'), path_separator = require('path').sep,

PATTERN_repository_path = /([^\/]+)\/(.+?)(?:-([^-].*))?$/;

// --------------------------------------------------------------------------------------------

if (typeof module === 'object') {
	// required as module
	module.exports = {
		parse_repository_path : parse_repository_path,
		installed_version : installed_version,
		get_GitHub_version : get_GitHub_version,
		check_version : check_version,
		// TODO: use Promise
		update : handle_arguments
	};

} else {
	// default action: update
	handle_arguments(process.argv[2], process.argv[3]);
}

function handle_arguments(repository_path, target_directory, callback) {
	if (repository_path ? PATTERN_repository_path.test(repository_path)
			: default_repository_path) {
		check_and_update(repository_path || default_repository_path,
		// run in CLI. GitHub 泛用的更新工具。
		target_directory, function(version_data, recover_working_directory,
				target_directory, update_script_path) {
			if (version_data.has_new_version) {
				// 在 repository 目錄下執行 post_install()
				(repository_path ? default_post_install_for_all
						: default_post_install)(target_directory,
						update_script_path);
				// 成功安裝了 repository 的組件。
				console.info('Successfully installed '
						+ version_data.repository);
			}
			// 之後回到原先的目錄底下。
			if (recover_working_directory)
				recover_working_directory();
			if (typeof callback === 'function')
				callback(version_data);
		});

	} else {
		console.log((repository_path ? 'Invalid repository: '
		// node GitHub.updater.node.js user/repository-branch [target_directory]
		+ JSON.stringify(repository_path) : '') + 'Usage:\n	'
				+ process.argv[0].replace(/[^\\\/]+$/)[0] + ' '
				+ process.argv[1].replace(/[^\\\/]+$/)[0]
				+ ' "user/repository-branch" ["target_directory"]'
				+ '\n\ndefault repository path: ' + default_repository_path);
	}
}

// --------------------------------------------------------------------------------------------

function detect_base_path(repository, branch) {
	var CeL_path_list;

	try {
		CeL_path_list = node_fs.readFileSync(repository_path_list_file)
				.toString();
	} catch (e) {
	}

	if (!CeL_path_list) {
		// ignore repository_path_list_file
		return;
	}

	var target_directory;

	// modify from _CeL.loader.nodejs.js
	CeL_path_list = CeL_path_list.split(CeL_path_list.includes('\n') ? /\r?\n/
			: '|');
	CeL_path_list.unshift('./' + repository + '-' + branch);
	// console.log(CeL_path_list);
	// 載入 CeJS 基礎泛用之功能。（非特殊目的使用的載入功能）
	CeL_path_list.some(function(path) {
		if (path.charAt(0) === '#'
		//
		&& path.endsWith(repository + '-' + branch)) {
			// path is comments
			return;
		}

		var matched = path
				.match(/(?:^|[\\\/])([a-z_\d]+)-([a-z_\d]+)[\\\/]?$/i);
		if (matched && (matched[1] !== repository || matched[2] !== branch)) {
			// 是其他 repository 的 path。
			return;
		}

		try {
			var fso_status = node_fs.lstatSync(path);
			if (fso_status.isDirectory()) {
				if (/^\.\.(?:$|[\\\/])/.test(path)
						&& !node_fs.existsSync('../ce.js'))
					return;
				target_directory = path;
				// console.info('detect_base_path: Use base path: ' + path);
				return true;
			}
		} catch (e) {
			// try next path
		}
	});

	return target_directory;
}

// --------------------------------------------------------------------------------------------

// parse repository path
function parse_repository_path(repository_path) {
	if (typeof repository_path === 'object' && repository_path.user_name
			&& repository_path.repository && repository_path.branch)
		return repository_path;

	/** {String}Repository name */
	var repository = repository_path.trim().match(PATTERN_repository_path),
	//
	user_name = repository[1], branch = repository[3] || 'master';
	repository = repository[2];

	return {
		user_name : user_name,
		repository : repository,
		branch : branch
	};
}

function installed_version(repository_path, callback, target_directory) {
	var original_working_directory, version_data = parse_repository_path(repository_path),
	//
	repository = version_data.repository, branch = version_data.branch;

	if (!target_directory) {
		target_directory = detect_base_path(repository, branch);
	} else if (!node_fs.existsSync(target_directory)) {
		node_fs.mkdirSync(target_directory);
	}
	if (target_directory) {
		// console.log('target_directory: ' + target_directory);
		target_directory = target_directory.replace(/[\\\/]+$/, '');
		if (target_directory
				&& (target_directory.endsWith(path_separator + repository + '-'
						+ branch) || target_directory.endsWith('\\'
						+ repository + '-' + branch))) {
			original_working_directory = process.cwd();
			process.chdir(target_directory.slice(0, -(path_separator
					+ repository + '-' + branch).length));
		}
		target_directory += path_separator;
	}

	var latest_version_file = repository + '-' + branch + '.version.json', has_version, has_version_data;
	console.info('Read latest version cache file ' + latest_version_file);
	try {
		has_version_data = JSON.parse(node_fs.readFileSync(latest_version_file)
				.toString());
		has_version = has_version_data.latest_version;
		// 不累積古老的(前前次)之 version_data。
		delete has_version_data.has_version_data;
	} catch (e) {
	}

	Object.assign(version_data, {
		check_date : new Date(),

		latest_version_file : latest_version_file,
		has_version_data : has_version_data,
		has_version : has_version
	});

	function recover_working_directory() {
		original_working_directory && process.chdir(original_working_directory);
	}

	if (typeof callback === 'function')
		try {
			callback(version_data, original_working_directory
			// recover working directory.
			&& recover_working_directory);
		} catch (e) {
			recover_working_directory();
		}
	else
		recover_working_directory();

	return version_data;
}

function get_GitHub_version(repository_path, callback, target_directory) {
	var version_data = parse_repository_path(repository_path), user_name = version_data.user_name, repository = version_data.repository, branch = version_data.branch;

	console.info('Get the infomation of latest version of '
	// 取得 GitHub 最新版本 infomation。
	+ repository + '...');
	node_https.get({
		// https://api.github.com/repos/kanasimi/CeJS/commits/master
		host : 'api.github.com',
		path : '/repos/' + user_name + '/' + repository + '/commits/' + branch,
		// https://developer.github.com/v3/#user-agent-required
		headers : {
			'user-agent' : 'CeL_updater/2.0'
		}
	}, function(response) {
		response.setTimeout(10000);
		var buffer_array = [], sum_size = 0;

		response.on('data', function(data) {
			sum_size += data.length;
			buffer_array.push(data);
		});

		response.on('end', function(e) {
			var contents = Buffer.concat(buffer_array, sum_size).toString(),
			//
			latest_commit = JSON.parse(contents),
			//
			latest_version = latest_commit.commit.author.date;

			Object.assign(version_data, {
				check_date : new Date(),

				latest_commit : latest_commit,
				latest_version : latest_version
			});

			callback(version_data);
		});
	})
	//
	.on('error', function(e) {
		// network error?
		console.error(e);
		callback(version_data);
	});
}

/**
 * detect repository version
 * 
 * @param {String}repository_path
 *            repository path. e.g., user/repository-branch
 * @param {Function}callback
 * @param {String}[target_directory]
 *            install repository to this local file system path.
 *            目標目錄位置。將會解壓縮至這個目錄底下。 default: repository-branch/
 */
function check_version(repository_path, callback, target_directory) {
	if (!repository_path) {
		throw 'No repository path specified!';
	}

	installed_version(repository_path, function(version_data,
			recover_working_directory) {
		get_GitHub_version(version_data, function(/* version_data */) {
			version_data.has_new_version
			//
			= version_data.has_version !== version_data.latest_version
					&& version_data.latest_version;

			callback(version_data, recover_working_directory);
		}, target_directory);
	}, target_directory);
}

function check_and_update(repository_path, target_directory, callback) {

	check_version(repository_path, function(version_data,
			recover_working_directory) {
		var has_version = version_data.has_version,
		//
		latest_version = version_data.latest_version;

		function recover(update_script_path) {
			if (typeof callback === 'function')
				callback(version_data, recover_working_directory,
						target_directory || '', update_script_path);
			else if (recover_working_directory)
				recover_working_directory();
		}

		if (version_data.has_new_version) {
			process.title = 'Update ' + repository_path;
			console.info('Update: '
					+ (has_version ? has_version + '\n     → ' : 'to ')
					+ latest_version);
			update_via_7zip(version_data, recover, target_directory);

		} else {
			console.info('Already have the latest version: ' + has_version);
			recover();
		}

	}, target_directory);

}

// --------------------------------------------------------------------------------------------

/**
 * determine what extract program to use.
 * 
 * @param {Array|String}extract_program_path
 *            Array: path list to test, String: using this path.
 */
function detect_extract_program_path(extract_program_path) {
	if (Array.isArray(extract_program_path)) {
		// detect 7zip path: 若是 $PATH 中有 7-zip 的可執行檔，應該在這邊就能夠被偵測出來。
		if (!extract_program_path.some(function(path) {
			// mute stderr
			// var stderr = process.stderr.write;
			// process.stderr.write = function() { };
			try {
				child_process.execSync(path + ' -h', {
					// pass I/O to the child process
					// https://nodejs.org/api/child_process.html#child_process_options_stdio
					stdio : 'ignore'
				});
			} catch (e) {
				path = null;
			}
			// process.stderr.write = stderr;
			return path && (extract_program_path = path);
		})) {
			// Can not find any extract program.
			extract_program_path = null;
		}
	}

	return extract_program_path;
}

function update_via_7zip(version_data, post_install, target_directory) {
	var latest_version = version_data.latest_version, user_name = version_data.user_name, repository = version_data.repository, branch = version_data.branch;

	extract_program_path = detect_extract_program_path(extract_program_path);
	if (!extract_program_path)
		// 'Please set up the extract_program_path first!'
		console.error('Please install 7-Zip first: https://www.7-zip.org/');

	// assert: typeof extract_program_path === 'string'

	// --------------------------------------------------------------------------------------------

	/** {String}下載之後將壓縮檔存成這個檔名。 */
	var target_file = repository + '-' + branch + '.zip';
	try {
		// 清理戰場。
		node_fs.unlinkSync(target_file);
	} catch (e) {
	}

	// 先確認/轉到目標目錄，才能 open file。
	var write_stream = node_fs.createWriteStream(target_file),
	// 已經取得的檔案大小
	sum_size = 0, start_time = Date.now(), total_size;

	function on_response(response) {
		// 採用這種方法容易漏失資料。 @ node.js v7.7.3
		// response.pipe(write_stream);

		// 可惜 GitHub 沒有提供 Content-Length，無法加上下載進度。
		total_size = +response.headers['content-length'];
		var buffer_array = [];

		response.on('data', function(data) {
			sum_size += data.length;
			buffer_array.push(data);
			process.stdout.write(target_file + ': ' + sum_size
			//
			+ (total_size ? '/' + total_size : '') + ' bytes ('
			// 00% of 0.00MiB
			+ (total_size ? (100 * sum_size / total_size | 0) + '%, ' : '')
			//
			+ (sum_size / 1.024 / (Date.now() - start_time)).toFixed(2)
					+ ' KiB/s)...\r');
		});

		response.on('end', function(e) {
			if (total_size && sum_size !== total_size) {
				console.error('Expected ' + total_size + ' bytes, but get '
						+ sum_size + ' bytes!');
			}
			write_stream.write(Buffer.concat(buffer_array, sum_size));
			// flush data
			write_stream.end();
		});
	}

	node_https.get('https://codeload.github.com/'
	// 取得 GitHub 最新版本壓縮檔案。
	+ user_name + '/' + repository + '/zip/' + branch, on_response)
	//
	.on('error', function(e) {
		// network error?
		// console.error(e);
		throw e;
	});

	// ---------------------------

	write_stream.on('close', function() {
		console.info(target_file + ': ' + sum_size
				+ ' bytes done. Extracting files to ' + process.cwd() + '...');

		// check file size
		var file_size = node_fs.statSync(target_file).size;
		if (file_size !== sum_size) {
			throw 'The file size ' + file_size + ' is not ' + sum_size
					+ '! Please try to run again.';
		}

		if (!extract_program_path) {
			throw 'Please extract the archive file manually: ' + target_file;
		}

		var command,
		//
		quoted_target_file = '"' + target_file + '"';
		if (extract_program_path.includes('unzip')) {
			command = extract_program_path + ' -t ' + quoted_target_file
					+ ' && '
					// 解開 GitHub 最新版本壓縮檔案 via unzip。
					+ extract_program_path + ' -x -o ' + quoted_target_file;
		} else {
			command = extract_program_path + ' t ' + quoted_target_file
					+ ' && '
					// 解開 GitHub 最新版本壓縮檔案 via 7z。
					+ extract_program_path + ' x -y ' + quoted_target_file;
		}

		child_process.execSync(command, {
			// pass I/O to the child process
			// https://nodejs.org/api/child_process.html#child_process_options_stdio
			stdio : 'inherit'
		});

		if (latest_version) {
			node_fs.writeFileSync(version_data.latest_version_file, JSON
					.stringify(version_data));

			try {
				// 解壓縮完成之後，可以不必留著程式碼檔案。 TODO: backup
				node_fs.unlinkSync(target_file);
			} catch (e) {
			}

			move_all_files_under_directory(repository + '-' + branch,
					target_directory, true);
			var update_script_path = (target_directory ? target_directory
					.replace(/[\\\/]+$/, '') : repository + '-' + branch)
					+ path_separator + default_update_script_directory;

			// 成功解壓縮。
			console.info('Successful decompression: ' + repository);

			if (typeof post_install === 'function')
				post_install(update_script_path);
		}

		// throw 'Some error occurred! Bad archive?';
	});

}

function simplify_path(path) {
	return path.replace(/[\\\/]+$/, '').replace(/^(?:\.\/)+/, '') || '.';
}

// 把 source_directory 下面的檔案全部搬移到 target_directory 下面去。
function move_all_files_under_directory(source_directory, target_directory,
		overwrite, create_empty_directory) {
	if (!target_directory)
		return 'NEEDLESS';

	function move(_source, _target) {
		var fso_list = node_fs.readdirSync(_source);
		if (!node_fs.existsSync(_target)
		// 對於空目錄看看是否要創建一個。
		&& (fso_list.length > 0 || create_empty_directory)) {
			node_fs.mkdirSync(_target);
		}
		_source += path_separator;
		_target += path_separator;
		fso_list.forEach(function(fso_name) {
			var fso_status = node_fs.lstatSync(_source + fso_name);
			if (fso_status.isDirectory()) {
				move(_source + fso_name, _target + fso_name);
			} else {
				if (node_fs.existsSync(_target + fso_name)) {
					if (overwrite)
						node_fs.unlinkSync(_target + fso_name);
					else
						return;
				}
				// console.log(_source + fso_name+'→'+ _target + fso_name);
				node_fs.renameSync(_source + fso_name, _target + fso_name);
			}
		});
		node_fs.rmdirSync(_source);
	}

	source_directory = simplify_path(source_directory);
	target_directory = simplify_path(target_directory);
	if (source_directory !== target_directory) {
		console.log('move_all_files_under_directory [' + source_directory
				+ ']→[' + target_directory + ']');
		move(source_directory, target_directory);
	}
}

// --------------------------------------------------------------------------------------------
// actions after file extracted

function default_post_install_for_all(base_directory) {
}

function default_post_install(base_directory, update_script_path) {
	if (false) {
		// using npm instead: require('gh-updater');
		console.info('Update the tool itself...');
		copy_library_file('gh-updater/GitHub.updater.node.js', null,
				base_directory);
	}

	console.info('Setup basic execution environment...');
	copy_library_file('_CeL.loader.nodejs.js', null, base_directory,
			update_script_path);
	try {
		// Do not overwrite repository_path_list_file.
		node_fs.accessSync(base_directory + repository_path_list_file,
				node_fs.constants.R_OK);
	} catch (e) {
		try {
			node_fs.renameSync(update_script_path
					+ repository_path_list_file.replace(/(\.[^.]+)$/,
							'.sample$1'), base_directory
					+ repository_path_list_file);
		} catch (e) {
			// TODO: handle exception
		}
	}
}

function copy_library_file(source_name, taregt_name, base_directory,
		update_script_path) {
	var taregt_path = (base_directory || '') + (taregt_name || source_name);
	try {
		node_fs.unlinkSync(taregt_path);
	} catch (e) {
		// TODO: handle exception
	}
	if (false)
		console.log('copy_library_file [' + update_script_path + source_name
				+ ']→[' + taregt_path + ']');
	node_fs.renameSync(update_script_path + source_name, taregt_path);
}
